#include "functions.hpp"

#include <iostream>     // cin/cout
#include <cstdlib>      // random numbers
using namespace std;

void DisplayMenu()
{
    cout << "------------------------------------------------" << endl;
    cout << "- MAIN MENU -" << endl;
    cout << "-------------" << endl;
    cout << "0. Run tests" << endl;
    cout << "1. PercentToDecimal" << endl;
    cout << "2. PricePlusTax" << endl;
    cout << "3. CountChange" << endl;
    cout << "4. Exit" << endl << endl;
}

int GetChoice(int min, int max)
{
    int choice;
    cout << "(Enter a number between " << min << " and " << max << "): ";
    cin >> choice;
    while( choice<min || choice>max)
    {
        cout << endl << "Invalid selection, please try again." << endl;
        cout << "(Enter a number between " << min << " and " << max << "): ";
        cin >> choice;
    }
    cout << endl << "Chose: " << choice << endl;

    return choice;
}

float PercentToDecimal(float percent)
{
    return percent/100;
}

float PricePlusTax(float price, float tax)
{
    return price + (price*tax);
}

float CountChange(int quarters, int dimes, int nickles, int pennies)
{
    return (quarters*.25)+(dimes*.1)+(nickles*.05)+(pennies*.01);
}
