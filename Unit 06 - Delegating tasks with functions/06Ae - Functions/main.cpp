#include <iostream>     // cin/cout
#include <string>       // string data type
using namespace std;

#include "functions.hpp"
#include "tests.hpp"

int main()
{
    bool done = false;
    int userChoice;
    while(!done)
    {
        DisplayMenu();
        userChoice = GetChoice(0,4);
        switch(userChoice)
        {
            case 0:
            {
                RunTests();
            }
            break;

            case 1:
            {
                float percent;
                cout << "-------------------------------" << endl;
                cout << "PercentToDecimal" << endl << endl;

                cout << "Enter a percent: ";
                cin >> percent;

                cout << "Decimal: " << PercentToDecimal(percent) << endl;
            }
            break;

            case 2:
            {
                float price;
                float tax;
                cout << "-------------------------------" << endl;
                cout << "PricePlusTax" << endl << endl;

                cout << "Enter price: ";
                cin >> price;
                cout << "Enter Tax: ";
                cin >> tax;

                cout << "Price plus tax: " << PricePlusTax(price,tax) << endl;
            }
            break;

            case 3:
            {
                int quarters;
                int dimes;
                int nickles;
                int pennies;
                cout << "-------------------------------" << endl;
                cout << "CountChange" << endl << endl;

                cout << "Enter # of quarters: ";
                cin >> quarters;
                cout << "Enter # of dimes: ";
                cin >> dimes;
                cout << "Enter # of nickles: ";
                cin >> nickles;
                cout << "Enter # of pennies: ";
                cin >> pennies;

                cout << "Total money: " << CountChange(quarters,dimes,nickles,pennies) << endl;
            }
            break;

            case 4:
            {
                done = true;
            }
            break;
        }
    }
    return 0;
}

