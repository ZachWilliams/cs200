#include <iostream>
#include <string>
using namespace std;

int count;
int choice;
bool done = false;
int sum = 0;

void Program1()
{
    count = 0;
    while(count<=20)
    {
        cout << count << endl;
        count++;
    }
}

void Program2()
{
    count = 1;
    while(count<=128)
    {
        cout << count << endl;
        count = count*2;
    }
}

void Program3()
{
    while(!done)
    {
        cout << "1. Withdraw" << endl << "2. Deposit" << endl << "3. Quit" << endl << "Choice: ";
        int choice;
        cin >> choice;
        if(choice == 1)
        {
            cout << "You withdrew $100" << endl;
        }
        else if(choice == 2)
        {
            cout << "You deposited $100" << endl;
        }
        else if(choice == 3)
        {
            done = true;
        }
    }
}

void Program4()
{
    while(!done)
    {
        cout << "enter a number from 1-10" << endl;
        cin >> choice;
        if(choice > 4)
        {
            cout << "Too high!" << endl << endl;
        }
        else if(choice < 4)
        {
            cout << "Too low!" << endl << endl;
        }
        else if(choice == 4)
        {
            cout << "Correct!" << endl;
            done = true;
        }
    }
}

void Program5()
{
    cout << "enter a number between 1-5: ";
    cin >> choice;
    while(choice < 1 || choice > 5)
    {
        cout << "Invalid entry, try again: ";
        cin >> choice;
    }
}

void Program6()
{
    count = 1;
    cout << "Enter a number for n: ";
    cin >> choice;
    while(count<=choice)
    {
        sum = sum + count;
        count++;
        cout << sum << endl;
    }
}

// Don't modify main
int main()
{
    while ( true )
    {
        cout << "Run which program? (1-6): ";
        cin >> choice;

        cout << endl << endl;
        cout << "------------------------------" << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }
        else if ( choice == 6 ) { Program6(); }

        cout << endl << endl;
        cout << "------------------------------" << endl;
    }

    return 0;
}
