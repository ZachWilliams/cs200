#include <iostream>
#include <string>
using namespace std;

int main()
{
    const float BASE_PRICE = 8.00;
    const float SAUCE_PRICE = 0.53;
    const float CHEESE_PRICE = 0.73;
    const float MEATS_PRICE = 1.25;
    const float VEGGIES_PRICE = 0.83;
    string pizza = "";
    float price = BASE_PRICE;
    int menuChoice;
    bool done = false;
    cout << "Pizza Maker" << endl;
    while(!done)
    {
        cout << endl << "Current pizza: " << pizza << endl;
        cout << "Price: " << price << endl << endl;
        cout << "TOPPINGS MENU" << endl;
        cout << "1. Sauces" << endl;
        cout << "2. Meats" << endl;
        cout << "3. Veggies" << endl;
        cout << "4. Extra cheese" << endl;
        cout << "5. Reset pizza" << endl;
        cout << "6. Checkout" << endl;
        cout << ">> ";
        cin >> menuChoice;

        if(menuChoice == 1)
        {
            cout << endl << "SAUCES MENU" << endl;
            cout << "1. Tomato" << endl;
            cout << "2. BBQ" << endl;
            cout << "3. Buffalo" << endl;
            cout << "4. Alfredo" << endl;
            cout << ">> ";
            cin >> menuChoice;
            if(menuChoice == 1)
            {
                pizza += "Tomato sauce, ";
                price += SAUCE_PRICE;
            }
            else if(menuChoice == 2)
            {
                pizza += "BBQ, ";
                price += SAUCE_PRICE;
            }
            else if(menuChoice == 3)
            {
                pizza += "Buffalo, ";
                price += SAUCE_PRICE;
            }
            else if(menuChoice == 4)
            {
                pizza += "Alfredo, ";
                price += SAUCE_PRICE;
            }
            else
            {
                cout << "INVALID MENU OPTION" << endl;
            }
        }
        else if(menuChoice == 2)
        {
            cout << endl << "MEATS MENU" << endl;
            cout << "1. Pepperoni" << endl;
            cout << "2. Sausage" << endl;
            cout << "3. Ham" << endl;
            cout << "4. Chicken" << endl;
            cout << ">> ";
            cin >> menuChoice;
            if(menuChoice == 1)
            {
                pizza += "Pepperoni, ";
                price += MEATS_PRICE;
            }
            else if(menuChoice == 2)
            {
                pizza += "Sausage, ";
                price += MEATS_PRICE;
            }
            else if(menuChoice == 3)
            {
                pizza += "Ham, ";
                price += MEATS_PRICE;
            }
            else if(menuChoice == 4)
            {
                pizza += "Chicken, ";
                price += MEATS_PRICE;
            }
            else
            {
                cout << "INVALID MENU OPTION" << endl;
            }
        }
        else if(menuChoice == 3)
        {
            cout << endl << "VEGGIES MENU" << endl;
            cout << "1. Mushrooms" << endl;
            cout << "2. Black olives" << endl;
            cout << "3. Pineapple" << endl;
            cout << "4. Tomatoes" << endl;
            cout << ">> ";
            cin >> menuChoice;
            if(menuChoice == 1)
            {
                pizza += "Mushrooms, ";
                price += VEGGIES_PRICE;
            }
            else if(menuChoice == 2)
            {
                pizza += "Black olives, ";
                price += VEGGIES_PRICE;
            }
            else if(menuChoice == 3)
            {
                pizza += "Pineapple, ";
                price += VEGGIES_PRICE;
            }
            else if(menuChoice == 4)
            {
                pizza += "Tomatoes, ";
                price += VEGGIES_PRICE;
            }
            else
            {
                cout << "INVALID MENU OPTION" << endl;
            }
        }
        else if(menuChoice == 4)
        {
            pizza += "Cheese, ";
            price += CHEESE_PRICE;
        }
        else if(menuChoice == 5)
        {
            pizza = "";
            price = BASE_PRICE;
        }
        else if(menuChoice == 6)
        {
            cout << endl << "CHECKOUT" << endl;
            cout << "Pizza: " << pizza << endl;
            cout << "Price: " << price << endl;
            done = true;
        }
        else
        {
            cout << "INVALID MENU OPTION" << endl;
        }
    }
}

