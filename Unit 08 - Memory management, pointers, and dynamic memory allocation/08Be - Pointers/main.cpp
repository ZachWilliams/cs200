// 08Be - Don't update this file

#include <string>
using namespace std;
#include "utilities/Menu.hpp"
#include "ProgramFunctions.hpp"

int main()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );
        int choice = Menu::ShowIntMenuWithPrompt( {
            "Exploring addresses",
            "Dereferencing pointers",
            "Pointers and classes"
        }, true, true );

        if      ( choice == 0 ) { done = true; }
        else if ( choice == 1 ) { Program1_ExploringAddresses(); }
        else if ( choice == 2 ) { Program2_DereferencingPointers(); }
        else if ( choice == 3 ) { Program3_PointersAndClasses(); }
    }
}
