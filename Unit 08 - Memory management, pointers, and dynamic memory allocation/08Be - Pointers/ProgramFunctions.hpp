#ifndef _PROGRAM_FUNCTIONS
#define _PROGRAM_FUNCTIONS

#include <iostream>
#include <string>
using namespace std;

void Program1_ExploringAddresses()
{
    /*
    The & symbol, when placed before a variable name, is known as the "address-of operator".
    It is used to get the address of that variable in memory.

    Pointer variables store addresses in memory, and so they are used to point to
    different variables' addresses at different times in a program.

    When a pointer is NOT in use, it is best practice to point it to "nullptr" to avoid memory errors.
    */

    Menu::Header( "Exploring addresses" );

    int int1 = 25, int2 = 50, int3 = 75;        // Integer variables.
    int * ptrInt = nullptr;                     // Pointer variable, initialized to nullptr for safety.
    bool done = false;
    while ( !done )
    {
        cout << "Integer values:" << endl;
        Menu::DrawTable<int>( { "int1", "int2", "int3" }, { int1, int2, int3 } );

        cout << endl;
        cout << "Integer addresses: " << endl;
        Menu::DrawTable<int*>( { "&int1", "&int2", "&int3" }, { &int1, &int2, &int3 } );

        cout << endl << "ptrInt is pointing to address: " << ptrInt << endl << endl;

        // Students implement this part
        int pointToWhich;
        cout << "Point ptrInt to which?" << endl
            << "1. Point ptrInt to int1's address" << endl
            << "2. Point ptrInt to int2's address" << endl
            << "3. Point ptrInt to int3's address" << endl
            << "4. Point ptrInt to nullptr" << endl
            << "0. Quit subprogram" << endl
            << ">> ";
        cin >> pointToWhich;
        cout << "You entered: " << pointToWhich << endl;

        // If they chose 0, quit the subprogram (done = true).
        // For 1, set the pointer ptrInt to point to the address of int1.
        // For 2, point to int2.
        // For 3, point to int3.
        // For 4, point to nullptr.

        if ( pointToWhich == 0 ) { done = true; }
        else if(pointToWhich==1)
        {
            ptrInt = &int1;
        }
        else if(pointToWhich==2)
        {
            ptrInt = &int2;
        }
        else if(pointToWhich==3)
        {
            ptrInt = &int3;
        }
        else if(pointToWhich==4)
        {
            ptrInt = nullptr;
        }

        cout << endl << endl;
    }
}

void Program2_DereferencingPointers()
{
    /*
    The * symbol, when written before a pointer variable's name, is known as the "dereference operator".

    When a pointer is pointing to some address in memory we can look at the
    address it's pointing to, but we can also look INTO that address to read and write
    data at that address. When we do this, it's called "dereferencing" the pointer.
    */

    Menu::Header( "Dereferencing pointers" );
    string student1 = "Anuraj S.", student2 = "Rahaf A.", student3 = "Alexa S.";    // String variables.
    string * ptrStudent = nullptr;                                                  // a string pointer, initialized to nullptr.
    string studentName;
    bool done = false;

    while ( !done )
    {
        cout << "Students:" << endl;
        Menu::DrawTable<string>(        { "student1",   "student2", "student3" },
                                        { student1,     student2,   student3 } );
        Menu::DrawTable<string*>( {},   { &student1,    &student2,  &student3 } );

        cout << endl << "ptrStudent is pointing to address: " << ptrStudent << endl << endl;

        // Students implement this part
        int choice;
        cout << "Do what?" << endl
            << "1. Point ptrStudent to student1's address" << endl
            << "2. Point ptrStudent to student2's address" << endl
            << "3. Point ptrStudent to student3's address" << endl
            << "4. Point ptrStudent to nullptr" << endl
            << "5. Read from ptrStudent" << endl
            << "6. Write to ptrStudent" << endl
            << "0. Quit subprogram" << endl
            << ">> ";
        cin >> choice;
        cout << "You entered: " << choice << endl;

        // For option 0, quit the subprogram (done = true).
        // For option 1, set the pointer ptrStudent to point to the address of student1.
        // For option 2, point to student2.
        // For option 3, point to student3.
        // For option 4, point to nullptr.
        // For option 5:
        //  * If ptrStudent is pointing to nullptr, display an error message.
        //  * otherwise, display the address that ptrStudent is pointing to and the value that address is storing.
        // For option 6:
        //  * If ptrStudent is pointing to nullptr, display an error message.
        //  * otherwise, ask the user to enter a new name for the student and store it via dereferencing ptrStudent.

        if ( choice == 0 ) { done = true; }
        else if(choice==1)
        {
            ptrStudent = &student1;
        }
        else if(choice==2)
        {
            ptrStudent = &student2;
        }
        else if(choice==3)
        {
            ptrStudent = &student3;
        }
        else if(choice==4)
        {
            ptrStudent = nullptr;
        }
        else if(choice==5)
        {
            if(ptrStudent==nullptr)
            {
                cout << "ERROR! Can not read from nullptr!" << endl;
            }
            else
            {
                cout << "Pointer Address:" << ptrStudent << endl;
                cout << "Pointer Value:" << *ptrStudent << endl;
            }
        }
        else if(choice==6)
        {
            if(ptrStudent==nullptr)
            {
                cout << "ERROR! Can not write to nullptr!" << endl;
            }
            else
            {
                cout << "Enter new student name: ";
                cin >> studentName;
                *ptrStudent = studentName;
            }
        }

        cout << endl << endl;
    }
}

struct Employee
{
    string name;
    float payPerHour;

    Employee( string n, float pph )
    {
        name = n;
        payPerHour = pph;
    }

    void Display()
    {
        cout << name << ", $" << payPerHour << "/hr" << endl;
    }
};

void Program3_PointersAndClasses()
{
    /*
    The -> operator is a special operator that does two things: It dereferences a class AND accesses a member of the class.
    If we have a pointer to a class, we can access a member like this:
        classPtr->Display();
    Or we could do it the long way:
        (*classPtr).Display();
    */
    Menu::Header( "Classes and Pointers" );

    Employee
        employee1( "Rai S.",    13.45 ),
        employee2( "Rose M.",   15.50 ),
        employee3( "Andre N.",  15.15 );
    Employee * ptrEmployee = nullptr;
    bool done = false;
    double enteredPay;
    string enteredName;
    int choice6;
    while ( !done )
    {
        cout << "Employees:" << endl;
        Menu::DrawTable<string>(        { "employee1",          "employee2",            "employee3" },
                                        { employee1.name,       employee2.name,         employee3.name } );
        Menu::DrawTable<float>( {},     { employee1.payPerHour, employee2.payPerHour,   employee3.payPerHour } );
        Menu::DrawTable<Employee*>( {}, { &employee1,           &employee2,             &employee3 } );

        cout << endl << "ptrEmployee is pointing to address: " << ptrEmployee << endl << endl;

        // Students implement this part
        int choice;
        cout << "Do what?" << endl
            << "1. Point ptrEmployee to employee1's address" << endl
            << "2. Point ptrEmployee to employee2's address" << endl
            << "3. Point ptrEmployee to employee3's address" << endl
            << "4. Point ptrEmployee to nullptr" << endl
            << "5. Read from ptrEmployee" << endl
            << "6. Write to ptrEmployee" << endl
            << "0. Quit subprogram" << endl
            << ">> ";
        cin >> choice;
        cout << "You entered: " << choice << endl;

        // For 0, quit the subprogram (done = true).
        // For 1, set ptrEmployee to point to the address of employee1.
        // For 2, point to employee2.
        // For 3, point to employee3.
        // For 4, point to nullptr.
        // For 5, do the following:
        //  * If ptrEmployee is pointing to nullptr, display an error message.
        //  * otherwise, call the Display() function of ptrEmployee.
        // For 6, do the following:
        //  * If ptrEmployee is pointing to nullptr, display an error message.
        //  * otherwise, ask if they want to (1) update the name, or (2) update the pay per hour.
        //  * if their choice was 1:
        //      * Ask them to enter a new employee name. Store it at ptrEmployee->name.
        //  * if their choice was 2:
        //      * Ask them to enter a new wage. Store it at ptrEmployee->payPerHour.

        if ( choice == 0 ) { done = true; }
        else if(choice==1)
        {
            ptrEmployee = &employee1;
        }
        else if(choice==2)
        {
            ptrEmployee = &employee2;
        }
        else if(choice==3)
        {
            ptrEmployee = &employee3;
        }
        else if(choice==4)
        {
            ptrEmployee = nullptr;
        }
        else if(choice==5)
        {
            if(ptrEmployee==nullptr)
            {
                cout << "ERROR! Can not read from nullptr!" << endl;
            }
            else
            {
                ptrEmployee->Display();
            }
        }
        else if(choice==6)
        {
            if(ptrEmployee==nullptr)
            {
                cout << "ERROR! Can not write to nullptr!" << endl;
            }
            else
            {
                cout << "Would you like to:" << endl << "(1) update the name" << endl << "(2) update the pay per hour" << endl << ">> ";
                cin >> choice6;
                if(choice6==1)
                {
                    cout << "Enter a new name: ";
                    cin >> enteredName;
                    cout << "test1" << endl;
                    ptrEmployee->name = enteredName;
                    cout << "test1" << endl;
                }
                else if(choice6==2)
                {
                    cout << "Enter the new pay per hour: ";
                    cin >> enteredPay;
                    ptrEmployee->payPerHour = enteredPay;
                }
            }
        }

        cout << endl << endl;
    }
}

#endif
