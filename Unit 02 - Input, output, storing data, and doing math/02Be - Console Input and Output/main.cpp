// 02Be Recipe Program by Zach Williams
#include <iostream>
using namespace std;

int main()
{
    cout << "Brownie Recipe" << endl << endl;
    cout << "Ingedients:" << endl;
    cout << "__________________________________" << endl;
    cout << "1/2 cup butter" << endl;
    cout << "1 cup white sugar" << endl;
    cout << "2 eggs" << endl;
    cout << "1 teaspoon vanilla extract" << endl;
    cout << "1/3 cup unsweetened cocoa powder" << endl;
    cout << "1/2 cup all-purpose flour" << endl;
    cout << "1/4 teaspoon salt" << endl;
    cout << "1/4 teaspoon baking powder" << endl;
    cout << "__________________________________" << endl << endl;
    cout << "Directions:" << endl;
    cout << "_____________________________________________________________________" << endl;
    cout << "1. Preheat oven to 350 degrees F (175 degrees C)." << endl << "   Grease and flour an 8-inch square pan." << endl << endl;
    cout << "2. In a large saucepan, melt 1/2 cup butter." <<endl << "   Remove from heat, and stir in sugar, eggs, and 1 teaspoon vanilla." << endl << "   Beat in 1/3 cup cocoa, 1/2 cup flour, salt, and baking powder." << endl << "   Spread batter into prepared pan." << endl << endl;
    cout << "3. Bake in preheated oven for 25 to 30 minutes. Do not overcook." << endl;
    cout << "_____________________________________________________________________" << endl;
    return 0;
}
