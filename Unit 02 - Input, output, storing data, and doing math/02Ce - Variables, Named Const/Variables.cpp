// 02Be Recipe Program by Zach Williams
#include <iostream>
using namespace std;

int main()
{
    //Constants
    const float CUP_BUTTER = .5;
    const float CUP_SUGAR = 1;
    const float EGGS = 2;
    const float TEASPOON_VANILLA = 1;
    const float CUP_COCOA = .33;
    const float CUP_FLOUR = .5;
    const float TEASPOON_SALT = .25;
    const float TEASPOON_BAKING_POWDER = .25;
    //variables
    float batches;
    //input batches
    cout << "Brownie Recipe" << endl;
    cout << "Enter nuber of batches: ";
    cin >> batches;
    cout << endl;
    //output recipe
    cout << "Ingedients:" << endl;
    cout << "__________________________________" << endl;
    cout << CUP_BUTTER * batches << " cup(s) butter" << endl;
    cout << CUP_SUGAR * batches << " cup(s) white sugar" << endl;
    cout << EGGS * batches << " egg(s)" << endl;
    cout << TEASPOON_VANILLA * batches << " teaspoon(s) vanilla extract" << endl;
    cout << CUP_COCOA * batches << " cup(s) unsweetened cocoa powder" << endl;
    cout << CUP_FLOUR * batches << " cup(s) all-purpose flour" << endl;
    cout << TEASPOON_SALT * batches << " teaspoon(s) salt" << endl;
    cout << TEASPOON_BAKING_POWDER * batches << " teaspoon(s) baking powder" << endl;
    cout << "__________________________________" << endl << endl;
    cout << "Directions:" << endl;
    cout << "_____________________________________________________________________" << endl;
    cout << "1. Preheat oven to 350 degrees F (175 degrees C)." << endl << "   Grease and flour an 8-inch square pan." << endl << endl;
    cout << "2. In a large saucepan, melt 1/2 cup butter." <<endl << "   Remove from heat, and stir in sugar, eggs, and 1 teaspoon vanilla." << endl << "   Beat in 1/3 cup cocoa, 1/2 cup flour, salt, and baking powder." << endl << "   Spread batter into prepared pan." << endl << endl;
    cout << "3. Bake in preheated oven for 25 to 30 minutes. Do not overcook." << endl;
    cout << "_____________________________________________________________________" << endl;
    return 0;
}
