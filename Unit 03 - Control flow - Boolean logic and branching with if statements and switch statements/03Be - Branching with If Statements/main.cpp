#include <iostream>
using namespace std;

int main()
{
    // -----------------------------------------------------------------------------------
    // ---------------------------- 1. IF statement --------------------------------------
    cout << endl << "PROGRAM 1" << endl;
    float bankBalance, withdrawAmount;
    cout << "enter balance: ";
    cin >> bankBalance;
    cout << "enter withdraw amount: ";
    cin >> withdrawAmount;
    bankBalance -= withdrawAmount;
    cout << "new balance: " << bankBalance << endl;
    if(bankBalance < 0)
    {
        cout << "(OVERDRAWN)" << endl;
    }

    // -----------------------------------------------------------------------------------
    // ---------------------------- 2. IF/ELSE statement ---------------------------------
    cout << endl << "PROGRAM 2" << endl;
    float earnedPoints, totalPoints;
    cout << "enter points earned: ";
    cin >> earnedPoints;
    cout << "enter total points available: ";
    cin >> totalPoints;
    float result = earnedPoints / totalPoints;
    if(result>=.5)
    {
        cout << "Pass" << endl;
    }
    else
    {
        cout << "Fail" << endl;
    }

    // -----------------------------------------------------------------------------------
    // ---------------------------- 3. IF/ELSE IF/ELSE statement -------------------------
    cout << endl << "PROGRAM 3" << endl;
    int charge;
    cout << "enter battery %: ";
    cin >> charge;
    if(charge >= 95)
    {
        cout << "[****]" << endl;
    }
    else if(charge >= 75)
    {
        cout << "[***_]" << endl;
    }
    else if(charge >= 50)
    {
        cout << "[**__]" << endl;
    }
    else if(charge >= 25)
    {
        cout << "[*___]" << endl;
    }
    else
    {
        cout << "[____]" << endl;
    }

    // -----------------------------------------------------------------------------------
    // ---------------------------- 4. And && operator -----------------------------------
    int choice;
    cout << endl << "PROGRAM 4" << endl;
    cout << "1. Ketsup" << endl << "2. Mustard" << endl << "3. Mayo" << endl << "4. Relish" << endl << "5. BBQ Sauce" << endl << "Select a condiment: ";
    cin >> choice;
    if (choice >= 1 && choice <= 5)
    {
        cout << "Good Choice!" << endl;
    }
    else
    {
        cout << "Invalid choice!" << endl;
    }

    // -----------------------------------------------------------------------------------
    // ---------------------------- 5. Or || operator ------------------------------------
    cout << endl << "PROGRAM 5" << endl;
    int number;
    cout << "enter a number from 1-10: ";
    cin >> number;
    if(number > 10 || number < 1)
    {
        cout << "Invalid choice!" << endl;
    }
    else
    {
        cout << "Valid choice!" << endl;
    }
    return 0;
}
