#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    char choice;
    float num1, num2, result;
    int numWhole1, numWhole2;
    cout << "Select math operation:" << endl;
    cout << "+ for addition" << endl << "- for subtraction" << endl << "* for multiplication" << endl << "/ for division" << endl << "p for power" << endl << "s for square root" << endl << endl;
    cout << "Choice: ";
    cin >> choice;

    switch(choice)
    {
        case '+':
        {
            cout << "Enter 2 numbers" << endl;
            cout << "Number 1: ";
            cin >> num1;
            cout << "Number 2: ";
            cin >> num2;
            cout << num1 << " + " << num2 << " = " << num1 + num2 << endl;
        }
        break;

        case '-':
        {
            cout << "Enter 2 numbers" << endl;
            cout << "Number 1: ";
            cin >> num1;
            cout << "Number 2: ";
            cin >> num2;
            cout << num1 << " - " << num2 << " = " << num1 - num2 << endl;
        }
        break;

        case '*':
        {
            cout << "Enter 2 numbers" << endl;
            cout << "Number 1: ";
            cin >> num1;
            cout << "Number 2: ";
            cin >> num2;
            cout << num1 << " * " << num2 << " = " << num1 * num2 << endl;
        }
        break;

        case '/':
        {
            cout << "Enter 2 numbers" << endl;
            cout << "Number 1: ";
            cin >> num1;
            cout << "Number 2: ";
            cin >> num2;
            cout << num1 << " / " << num2 << " = " << num1 / num2 << endl;
        }
        break;

        case 'p':
        {
            cout << "Enter 2 whole numbers, 1 as a base, and 1 as an exponent" << endl;
            cout << "Base: ";
            cin >> numWhole1;
            cout << "Exponent: ";
            cin >> numWhole2;
            result = pow(numWhole1, numWhole2);
            cout << numWhole1 << "^" << numWhole2 << " = " << result << endl;
        }
        break;

        case 's':
        {
            cout << "enter a whole number: ";
            cin >> numWhole1;
            result = sqrt(numWhole1);
            cout << "The square root of " << numWhole1 << " is: " << result << endl;
        }
        break;

        default:
        {
            cout << "Invalid operation" << endl;
        }
    }
}
