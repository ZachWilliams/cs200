#ifndef _FITB_HPP
#define _FITB_HPP
#include "Question.hpp"

#include <string>
using namespace std;

class FillInTheBlankQuestion : public Question
{
    public:
        void SetAnswer(string answer);
        bool AskQuestion();

    protected:
        string m_answer;
};

#endif
