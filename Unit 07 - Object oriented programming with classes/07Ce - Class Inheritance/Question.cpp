#ifndef _QUESTION_CPP
#define _QUESTION_CPP
#include "Question.hpp"

#include <string>
#include <iostream>
using namespace std;

void Question::SetQuestion(string question)
{
    m_question = question;
}

void Question::DisplayQuestion()
{
    cout << endl << "QUESTION" << endl;
    cout << m_question << endl << endl;
}

#endif
