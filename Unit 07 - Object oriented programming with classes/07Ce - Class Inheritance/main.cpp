#include "Question.hpp"
#include "TrueFalseQuestion.hpp"
#include "MultipleChoiceQuestion.hpp"
#include "FillInTheBlankQuestion.hpp"

#include <iostream>
using namespace std;

int main()
{
    int score = 0;
    int totalQuestions = 3;
    string question3Answers[] = {"Green", "Red", "Blue", "Yellow"};

    FillInTheBlankQuestion question1;
    TrueFalseQuestion question2;
    MultipleChoiceQuestion question3;

    question1.SetQuestion("What is the color of grass?");
    question1.SetAnswer("Green");
    question2.SetQuestion("Is the sky blue?");
    question2.SetAnswer(true);
    question3.SetQuestion("What color is water?");
    question3.SetAnswers(question3Answers, 2);

    if(question1.AskQuestion()==true)
    {
        score++;
    }
    if(question2.AskQuestion()==true)
    {
        score++;
    }
    if(question3.AskQuestion()==true)
    {
        score++;
    }

    cout << endl << "RESULTS" << endl;
    cout << score << " correct out of " << totalQuestions;

    return 0;
}
