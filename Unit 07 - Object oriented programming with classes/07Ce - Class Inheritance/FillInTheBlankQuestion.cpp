#ifndef _FITB_CPP
#define _FITB_CPP
#include "FillInTheBlankQuestion.hpp"

#include <string>
#include <iostream>
using namespace std;

void FillInTheBlankQuestion::SetAnswer(string answer)
{
    m_answer = answer;
}

bool FillInTheBlankQuestion::AskQuestion()
{
    string userAnswer;
    DisplayQuestion();

    cout << "Your answer: ";
    cin >> userAnswer;

    if(m_answer==userAnswer)
    {
        cout << endl << "Correct!" << endl;
    }
    else
    {
        cout << endl << "Incorrect!" << endl;
    }
}

#endif
