#ifndef _TF_CPP
#define _TF_CPP
#include "TrueFalseQuestion.hpp"

#include <string>
#include <iostream>
using namespace std;

void TrueFalseQuestion::SetAnswer(bool answer)
{
    m_answer = answer;
}

bool TrueFalseQuestion::AskQuestion()
{
    int userAnswer;
    DisplayQuestion();

    cout << "0. false" << endl;
    cout << "1. true" << endl;
    cout << "Your answer: ";
    cin >> userAnswer;

    if(m_answer==userAnswer)
    {
        cout << endl << "Correct!" << endl;
    }
    else
    {
        cout << endl << "Incorrect!" << endl;
    }
}

#endif
