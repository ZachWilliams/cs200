#ifndef _TF_HPP
#define _TF_HPP
#include "Question.hpp"

#include <string>
using namespace std;

class TrueFalseQuestion : public Question
{
    public:
        void SetAnswer(bool answer);
        bool AskQuestion();

    protected:
        bool m_answer;
};

#endif
