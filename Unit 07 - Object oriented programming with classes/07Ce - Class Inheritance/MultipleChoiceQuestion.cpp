#ifndef _MC_CPP
#define _MC_CPP
#include "MultipleChoiceQuestion.hpp"

#include <string>
#include <iostream>
using namespace std;

void MultipleChoiceQuestion::SetAnswers(string answers[4], int correctIndex)
{
    m_correctIndex = correctIndex;

    for(int i=0;i<4;i++)
    {
        m_answers[i] = answers[i];
    }
}

bool MultipleChoiceQuestion::AskQuestion()
{
    int userAnswer;
    DisplayQuestion();

    cout << "0. " << m_answers[0] << endl;
    cout << "1. " << m_answers[1] << endl;
    cout << "2. " << m_answers[2] << endl;
    cout << "3. " << m_answers[3] << endl;
    cout << "Your answer: ";
    cin >> userAnswer;

    if(m_correctIndex==userAnswer)
    {
        cout << endl << "Correct!" << endl;
    }
    else
    {
        cout << endl << "Incorrect!" << endl;
    }
}

#endif
