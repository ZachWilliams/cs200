#include <iostream>
#include <cstdlib>
#include <ctime>
#include "Die.hpp"
#include "Card.hpp"
using namespace std;

int main()
{
    bool done;
    int choice = 0;
    while(!done)
    {
        cout << endl << "-----------------------" << endl;
        cout << "       MAIN MENU       " << endl;
        cout << "-----------------------" << endl;
        cout << "1. Roll Die" << endl;
        cout << "2. Draw Cards" << endl;
        cout << "3. Quit" << endl << endl;
        while(choice<1 || choice>3)
        {
            cout << "(1-3): ";
            cin >> choice;
        }
        if(choice==1)
        {
            int sides = 0;
            cout << "Dice" << endl;
            while(sides<1)
            {
                cout << "Enter amount of sides for dice 1: ";
                cin >> sides;
            }
            Die die1(sides);
            cout << "Dice 1: " << die1.Roll() << endl;
            cout << "Dice 1: " << die1.Roll() << endl;
            cout << "Dice 1: " << die1.Roll() << endl;

            sides = 0;
            while(sides<1)
            {
                cout << "Enter amount of sides for dice 2: ";
                cin >> sides;
            }
            Die die2(sides);
            cout << "Dice 1: " << die2.Roll() << endl;
            cout << "Dice 1: " << die2.Roll() << endl;
            cout << "Dice 1: " << die2.Roll() << endl;

            choice = 0;
        }
        else if (choice==2)
        {
            string rank1;
            string rank2;
            char suit1;
            char suit2;
            Card card1;
            Card card2;

            cout << "Enter rank for card 1: ";
            cin >> rank1;
            cout << "Enter suit for card 1: ";
            cin >> suit1;
            cout << "Enter rank for card 2: ";
            cin >> rank2;
            cout << "Enter suit for card 2: ";
            cin >> suit2;

            card1.SetRank(rank1);
            card2.SetRank(rank2);
            card1.SetSuit(suit1);
            card2.SetSuit(suit2);

            cout << "Card 1: ";
            card1.Display();
            cout << "Card 2: ";
            card2.Display();

            choice = 0;
        }
        else if(choice==3)
        {
            done = true;
        }
    }


    return 0;
}
