#ifndef _DIE_HPP
#define _DIE_HPP

class Die
{
    public:
        Die();
        Die(int sideCount);
        int Roll();

    private:
        int sides;
};

#endif
