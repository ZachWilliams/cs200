#include <iostream>
using namespace std;

#include "Recipe.hpp"
#include "Ingredient.hpp"

int main()
{
    Recipe recipe;
    bool done = false;
    int choice;
    string temp;

    while(!done)
    {
        cout << "RECIPE EDITOR" << endl << endl;
        cout << "Editing recipe:" << endl << endl;
        recipe.Display();
        cout << endl << "Main menu:" << endl;
        cout << "1. Set name" << endl;
        cout << "2. Set instructions" << endl;
        cout << "3. Set source" << endl;
        cout << "4. Add ingredient" << endl;
        cout << "5. Save" << endl;
        cout << "6. Quit" << endl << endl;
        cout << ">> " ;
        cin >> choice;

        if(choice==1)
        {
            cout << "SET NAME" << endl << "----------------------------------" << endl << "Enter recipe name: ";
            cin >> temp;
            recipe.SetName(temp);
        }
        else if(choice==2)
        {
            cout << "SET INSTRUCTIONS" << endl << "----------------------------------" << endl << "Enter instructions: ";
            cin >> temp;
            recipe.SetInstructions(temp);
        }
        else if(choice==3)
        {
            cout << "SET SOURCE" << endl << "----------------------------------" << endl << "Enter source: ";
            cin >> temp;
            recipe.SetSource(temp);
        }
        else if(choice==4)
        {
            string iName;
            float iAmount;
            string iUnit;
            cout << "ADD INGREDIENT" << endl << "----------------------------------" << endl << "Enter ingredient name: ";
            cin >> iName;
            cout << "Enter unit of measurement: ";
            cin >> iUnit;
            cout << "Enter amount: ";
            cin >> iAmount;
            recipe.AddIngredient(iName,iAmount,iUnit);
        }
        else if(choice==5)
        {
            cout << "SAVE" << endl << "----------------------------------" << endl << "Enter a filename to save it under (include .txt at the end): ";
            cin >> temp;
            recipe.Save(temp);
        }
        else if(choice==6)
        {
            cout << "QUIT" << endl << "----------------------------------" << endl << "Goodbye!";
            done = true;
        }
        else
        {
            cout << "Invalid choice!" << endl;
        }
    }

    return 0;
}
