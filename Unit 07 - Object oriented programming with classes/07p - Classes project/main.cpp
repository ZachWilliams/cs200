#include <iostream>
using namespace std;

#include "Cookbook.hpp"

int main()
{
    Cookbook cookbook;
    cookbook.LoadRecipes("recipes.txt");
    cookbook.SetTitle( "Zach's Cookbook" );

    bool done = false;
    int choice;

    while(!done)
    {
        cout << endl;
        cout << cookbook.GetTitle();
        cout << endl << "------------------------------------" << endl;
        cout << "Which recipe would you like to view?" << endl;
        cookbook.DisplayRecipeList();
        cout << "Or -1 to quit." << endl << endl;
        cout << ">> " ;
        cin >> choice;

        if(choice==-1)
        {
            done = true;
        }
        else
        {
            cookbook.DisplayRecipe(choice);
        }
    }
    return 0;
}
