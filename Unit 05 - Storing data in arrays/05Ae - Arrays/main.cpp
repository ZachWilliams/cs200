#include <iostream>
#include <string>
using namespace std;

void Program1()
{
    const int MAX_COURSES=6;
    int courseCount=0;
    string myCourses[MAX_COURSES];
    string userInput;
    for(int i=0; i<MAX_COURSES; i++)
    {
        cout << "Please enter a class name or 'STOP' to finish: ";
        cin >> userInput;
        if(userInput=="STOP")
        {
            break;
        }
        myCourses[i] = userInput;
        courseCount++;
    }
    cout << "Your classes:" << endl;
    for(int i=0; i<MAX_COURSES; i++)
    {
        cout << myCourses[i] << " ";
    }
    cout << endl;
}

void Program2()
{
    string names[5];
    float prices[5];
    for(int i=0; i<5; i++)
    {
        cout << "ITEM " << i << endl << "   Enter the Item's name: ";
        cin >> names[i];
        cout << "   Enter the item's price: ";
        cin >> prices[i];
    }
    cout << "Items for sale:" << endl;
    for(int i=0; i<5; i++)
    {
        cout << "   " << "Item:" << endl;
        cout << "       " << names[i] << ", $" << prices[i] << endl;
    }
}

void Program3()
{
    const int WIDTH = 30;
    const int HEIGHT = 10;
    string gameMap[WIDTH][HEIGHT];
    for ( int y = 0; y < HEIGHT; y++ )
    {
        for ( int x = 0; x < WIDTH; x++ )
        {
            if(x==0 || x==29 || y==0 || y==9)
            {
                gameMap[x][y] = "#";
            }
            else
            {
                gameMap[x][y] = ".";
            }
        }
    }
    gameMap[15][8] = "@";
    gameMap[21][5] = "%";
    gameMap[9][3] = "&";
    gameMap[7][2] = "?";
    gameMap[27][6] = "!";
    for ( int y = 0; y < HEIGHT; y++ )
    {
        for ( int x = 0; x < WIDTH; x++ )
        {
            cout << gameMap[x][y];
        }
        cout << endl;
    }
}

int main()
{
    bool done = false;

    while ( !done )
    {
        int choice;
        cout << "0. QUIT" << endl;
        cout << "1. Program 1" << endl;
        cout << "2. Program 2" << endl;
        cout << "3. Program 3" << endl;
        cout << endl << ">> ";
        cin >> choice;

        switch( choice )
        {
            case 0: done = true; break;
            case 1: Program1(); break;
            case 2: Program2(); break;
            case 3: Program3(); break;
        }
    }

    return 0;
}

